package pl.ucraft.testvip;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

public class TestvipPlugin extends JavaPlugin {

    static TestvipPlugin plugin;

    public void onEnable(){
        this.saveDefaultConfig();
        plugin = this;

        Bukkit.getServer().getPluginCommand("testvip").setExecutor(new TestvipCommand());
    }

    public void onDisable(){
        this.saveConfig();
    }

    public static boolean isTaken(UUID uuid){
        return plugin.getConfig().isSet(uuid.toString());
    }

    public static void setTaken(UUID uuid){
        plugin.getConfig().set(uuid.toString(), true);
        plugin.saveConfig();
    }

}
