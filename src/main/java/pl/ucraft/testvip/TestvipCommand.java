package pl.ucraft.testvip;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TestvipCommand implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        Player player = (Player) commandSender;
        if(!TestvipPlugin.isTaken(player.getUniqueId())){
            player.sendMessage(ChatColor.GRAY + "Testowy VIP przyznany na 48 godzin.");
            Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), "pex user " + player.getName().toLowerCase() + " group add VIP * 172800");
            TestvipPlugin.setTaken(player.getUniqueId());
        }else{
            player.sendMessage(ChatColor.RED + "Odebrales juz swojego darmowego vipa.");
        }
        return true;
    }
}